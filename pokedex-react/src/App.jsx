import { About, Dashboard, Home, NotFoundPage, Pokedex, PokemonDetail } from './pages';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import { Link, Route, Routes, BrowserRouter as Router, useNavigate} from 'react-router-dom';
import { connect } from 'react-redux';
import { useState, useEffect, useContext } from 'react';
import FavPokemonContext from './contexts/PokemonContext';
import './App.scss';



function App({user, error}) {
  const [authError, setAuthError] = useState(null);
  const [favPokemon, setFavPokemon] = useState([]);

  useEffect(() =>{
    setAuthError(error)
  },[error])

  return (
    <FavPokemonContext.Provider value = {favPokemon}>
      <div className="App">
        <header className='header'>
          <div className="header__link-home">
            {/* IS ACTIVE = CLASSNAME */}
          <Link to="/" onClick={(isActive) => console.log(isActive.isTrusted)}>Inicio</Link>
          </div>
          <div className="header__link-pokedex">
          <Link to="/pokedex">Pokédex</Link>
          </div>
          <div className="header__link-about">
          <Link to="/about">Sobre nosotros</Link>
          </div>
          {(user) ? <div className='header__link-profile'><Link to="/dashboard">Perfil</Link></div> : '' }
        </header>
        <Routes>
          <Route path="/">
            <Route path="/" element={<Home/>}/></Route>
            <Route path="/pokedex" element={<Pokedex user = {user}/>}></Route>
            <Route path="/pokedex/:name" element={<PokemonDetail/>}></Route>
            <Route path="/dashboard" element={<PrivateRoute user={user} error={authError} component={<Dashboard user={user}/>}/>}> </Route>
            <Route path="/about" element={<About/>}></Route>
            <Route path="/*" element={<NotFoundPage/>}></Route>
        </Routes>
      </div>
    </FavPokemonContext.Provider>
  );
}
const mapStateToProps = (state) =>({
  user: state.auth.user,
  error: state.auth.error
})
export default connect(mapStateToProps)(App);
