import React from 'react'

const favPokemons = [];

const FavPokemonContext = React.createContext(favPokemons);

export default FavPokemonContext;