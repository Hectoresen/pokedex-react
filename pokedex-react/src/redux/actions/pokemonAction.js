export const GET_POST = 'GET_POST';
export const GET_POST_OK = 'GET_POST_OK';
export const GET_POST_ERROR = 'GET_POST_ERROR';
export const GET_POKEMON_DETAIL_OK = 'GET_POKEMON_DETAIL_OK';

const actionGetPost = () => ({
    type: GET_POST,
});

const actionGetPostOk = (post) => ({
    type: GET_POST_OK,
    payload: post,
});
const actionGetPokemonDetail = (poke) =>({
    type: GET_POKEMON_DETAIL_OK,
    payload: poke,
})

const actionGetPostError = () => ({
    type: GET_POST_ERROR
})

export const getPokemon = (pokemonName) => {
    return async (dispatch) => {
        dispatch(actionGetPost());

        if(pokemonName){
        try {
            const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
            const data = await res.json();
            dispatch(actionGetPokemonDetail(data));
        } catch(error) {
            console.log(error);
            dispatch(actionGetPostError());
        }
        }else{
            try {
                const res = await fetch('https://pokeapi.co/api/v2/pokemon/')
                const data = await res.json();
                dispatch(actionGetPostOk(data));
            } catch(error) {
                console.log(error);
                dispatch(actionGetPostError());
            }
        }
    };
};
