export const AUTH_REGISTER = "AUTH_REGISTER";
export const AUTH_REGISTER_OK = "AUTH_REGISTER_OK";
export const AUTH_REGISTER_ERROR = "AUTH_REGISTER_ERROR";

export const AUTH_LOGIN = "AUTH_LOGIN";
export const AUTH_LOGIN_OK = "AUTH_LOGIN_OK";
export const AUTH_LOGIN_ERROR = "AUTH_LOGIN_ERROR";

export const CHECK_SESSION = "CHECK_SESSION";
export const CHECK_SESSION_OK = "CHECK_SESSION_OK";
export const CHECK_SESSION_ERROR = "CHECK_SESSION_ERROR"

export const registerUser = (form) => {

    return async (dispatch) => {
    dispatch({ type: AUTH_REGISTER });

    const checkUsersDB = await fetch('https://pokedex-579ba-default-rtdb.europe-west1.firebasedatabase.app/users.json')
    const data = await checkUsersDB.json();
    const dataResults = Object.values(data);

    const exitsUser = dataResults.find((user) =>{
        return (user.email === form.email)})

    if(exitsUser){
            console.log('Ya existe un usuario con ese correo electrónico', exitsUser);
            dispatch({type: AUTH_REGISTER_ERROR, payload: 'Ya existe un usuario con el mismo correo electrónico'})
        }else{
            const request = await fetch("https://pokedex-579ba-default-rtdb.europe-west1.firebasedatabase.app/users.json", {
                method: "POST",
                headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
            },
                body: JSON.stringify(form),
            });
            if (request.ok) {
                    dispatch({ type: AUTH_REGISTER_OK, payload: form });
                } else {
                    dispatch({ type: AUTH_REGISTER_ERROR, payload: false });
                }
        }
    };
};

export const loginUser = (form) =>{
    return async (dispatch) =>{
        dispatch({type: AUTH_LOGIN});
        const res = await fetch('https://pokedex-579ba-default-rtdb.europe-west1.firebasedatabase.app/users.json')
        const data = await res.json();
        const usersData = Object.values(data);
        const activeUser = usersData.find((user) =>{
            return (user.email === form.email) && (user.password === form.password) })

        if(activeUser) dispatch({type: AUTH_LOGIN_OK, payload: activeUser})
        if(!activeUser) dispatch({type: AUTH_LOGIN_ERROR, payload: 'Credenciales incorrectas'})
    }
}

