import { combineReducers } from "redux";
import pokemonReducer from "./pokemonReducer";
import { authReducer } from "./authReducer";

const rootReducer = combineReducers({
    pokemon: pokemonReducer,
    auth: authReducer,
});

export default rootReducer;