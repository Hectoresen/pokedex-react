import * as actions from "../actions/pokemonAction";

const INITIAL_STATE = {
    pokemon: [],
    pokemonDetail: {},
    loading: false,
    error: false,
};

const pokemonReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actions.GET_POST: {
            return { ...state, loading: true };
        }
        case actions.GET_POST_OK: {
            return { ...state, pokemon: action.payload, loading: false, error: false };
        }
        case actions.GET_POKEMON_DETAIL_OK: {
            return { ...state, pokemonDetail: action.payload, loading: false, error: false};
        }
        case actions.GET_POST_ERROR: {
            return {...state, error: true, loading: false}
        }
        default:
            return state;
}
};

export default pokemonReducer;
