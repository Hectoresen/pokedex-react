import { Modal, Button, Input, Text, Container, Row, Col, Checkbox  } from '@nextui-org/react';
import React from 'react';
import { MdFavorite } from 'react-icons/md';
import { useState } from 'react';



const FavModal = () => {
    const [visible, setVisible] = React.useState(false);
    const handler = () => setVisible(true);
    const closeHandler = () => {
        setVisible(false);
        console.log('closed');
    };
    return (
    <div>
        <button
            className='pokemon-list-btn'
            onClick={handler}>
                <MdFavorite />Hazlo favorito!
        </button>
        <Modal
            closeButton
            aria-labelledby="modal-title"
            open={visible}
            onClose={closeHandler}
        >
            <Modal.Header>
                <Text id="modal-title" size={18}>
                Bienvenid@ a nuestra
                <Text b size={18}>
                Pokedex!
                </Text>
                </Text>
            </Modal.Header>
            <Modal.Body>
            🔔 Debes iniciar sesión para añadir tus pokémons favoritos
            </Modal.Body>
        </Modal>
    </div>
    );
    }

export default FavModal;