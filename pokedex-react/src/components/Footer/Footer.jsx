import {BsInstagram} from 'react-icons/bs';
import './footer.scss';

const Footer = () =>{
    return <footer className='footer'>
        <div className='footer__socials'>
            <div className='footer__socials-instagram'>
                <p>Síguenos en instagram!</p>
            <BsInstagram/>
            </div>
        </div>
    </footer>
}
export default Footer;