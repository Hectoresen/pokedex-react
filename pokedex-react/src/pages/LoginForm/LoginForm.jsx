import { useState}  from 'react';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { RegisterForm } from '..';
import { loginUser } from '../../redux/actions/authActions';
import { Input, Button } from '@nextui-org/react';
import {GrUserAdd} from 'react-icons/gr';
import './login.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
}

const LoginForm = ({dispatch, error, user}) =>{
    console.log(error)
    const navigate = useNavigate();
    const [formData, setFormData] = useState(INITIAL_STATE);
    const [notRegistered, setNotRegistered] = useState(false);

    const inputChange = (ev) =>{
        const {name, value} = ev.target;
        setFormData({ ...formData, [name]: value});
    }
    const submitForm = (ev) =>{
        ev.preventDefault();
        dispatch(loginUser(formData));
        (error) ? <div>Credenciales incorrectas</div> : navigate('/dashboard');
    }
    return (notRegistered)
            ?
            <RegisterForm needRegister={notRegistered}/>
            :
            <div className='form'>
                <div className='form__login'>
                    <form onSubmit={submitForm}>
                        <label>
                            <p>INICIAR SESIÓN</p>
                            <Input bordered labelPlaceholder="Correo electrónico" color="secondary" type='email' name='email' onChange={inputChange} />
                        </label>
                        <label>
                            <Input bordered labelPlaceholder="Contraseña" color="secondary" type='password' name='password' onChange={inputChange} />
                        </label>
                        <div>
                        <Button bordered color="secondary" auto>
                            Iniciar sesión
                        </Button>
                        </div>
                    </form>
                    <div className='form__login-register'>
                        <button onClick={() =>{setNotRegistered(true)}}><GrUserAdd/> Necesito registrarme</button>
                    </div>
                </div>
            </div>
}

const mapStateToProps = (state) =>({
    error: state.auth.error,
    user: state.auth.user,
})
export default connect(mapStateToProps)(LoginForm);