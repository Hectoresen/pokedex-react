import { getPokemon } from '../../redux/actions/pokemonAction';
import FavModal from '../../components/FavModal/FavModal';
import { connect } from 'react-redux';
import { useEffect, React, useContext, useState} from 'react';
import { PokemonFinder } from "..";
import {Link} from 'react-router-dom';
import FavPokemonContext from '../../contexts/PokemonContext';
import { MdFavorite } from 'react-icons/md';
import Alert from '@mui/material/Alert';
import './gallery.scss';



const Gallery = (props) =>{
    const [show, setShow] = useState(false)

    useEffect(() =>{
        props.dispatch(getPokemon());
    }, []);
    const favPokemon = useContext(FavPokemonContext)

    const addFavPokemons = (poke) =>{
        favPokemon.push(poke);
        setShow(true)
        /* Comprobar si ya existe, si no existe, hacer un push y enseñar línea 65 con un time out */
        setTimeout(resetAlertState, 2000);
    }
    const resetAlertState = () =>{
        setShow(false);
    }


    const showContent = () =>{
        const {loading, error, pokemon, user} = props;
        const {results} = props.pokemon;

        if(loading) return <div>Loading...</div>
        if(error) return <div>Ha ocurrido un error...</div>
        if(results){
            return results.map(poke=>
                <div
                key={poke.name}
                className="pokemon-list"
                >{poke.name}
                <div className='pokemon-list__container__btn'>
                <div className='pokemon-list__container__btn-imgLink'>
                <Link to ={`/pokedex/${poke.name}`}>Ver</Link>
                </div>
                </div>
                {(user)
                ?
                <button
                className='pokemon-list-btn'
                onClick={() => addFavPokemons(poke)}
                /* onClick={() =>{favPokemon.push(poke)}} */>
                    <MdFavorite />Hazlo favorito!</button>
                :
                <FavModal/>
                }
                </div>
                );
        }
    };

    return <div className="main-container__results">
        <PokemonFinder pokeData = {props.pokemon}/>
        <div className="main-container__results-poke">
            <div className='alert-fav'>
                {(show) ? <Alert severity="success">Pokemon añadido a favoritos!</Alert> : ''}
            </div>
        {showContent()}
        </div>
    </div>
}

const mapStateToProps = ({pokemon: {loading, pokemon, error}}) =>({
    loading: loading,
    pokemon: pokemon,
    error: error
})

export default connect(mapStateToProps)(Gallery);