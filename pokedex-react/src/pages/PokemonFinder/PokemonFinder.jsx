import { useState } from "react";
import { TiDelete} from 'react-icons/ti';
import './pokemonFinder.scss';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'



const PokemonFinder = (props) =>{
    const {results} = props.pokeData;

    const INITIAL_STATE = {
        name: '',
    }
    const [pokeResults, setPokeResults] = useState([]);
    const [showDeleteList, setShowDeleteList] = useState(false);

    const inputChange = (ev) =>{
        results.map(poke =>{
            if(poke.name.includes(ev.target.value.toLowerCase()) && !pokeResults.includes(poke)){
                setPokeResults([ ...pokeResults, poke]);
                setShowDeleteList(true);
            }
        })
    };
    const deleteItems = (ev) =>{
        setPokeResults([]);
        setShowDeleteList(false);
        /* FALTA LIMPIAR EL INPUT */
    }

    return <div className='pokemonFinder'>
            <div className='pokemonFinder-input'>
                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label>Encuentra tu Pokémon!</Form.Label>
                        <Form.Control type="text" name="name" onChange={inputChange}/>
                    </Form.Group>
                </Form>
            </div>
            <div className="pokemonFinder-results">
                {(showDeleteList) ? <div className="pokemonFinder-results-delete"><button onClick={deleteItems}><TiDelete/></button></div> : ''}
                {pokeResults.map(poke=>
                <div
                key={poke.id, poke.name}
                className="pokemonFinder-results-poke">
                    {poke.name}
                    {poke.id}
                    <Button
                        id= "send-btn"
                        variant= "dark"
                        className="pokemon-list-btn"
                        >Ver{/* <a href={`https://www.wikidex.net/wiki/${poke.name}`} target="_blank">Ver Pokémon</a> */}
                    </Button>
                </div>)}
            </div>
        </div>
}

export default PokemonFinder;