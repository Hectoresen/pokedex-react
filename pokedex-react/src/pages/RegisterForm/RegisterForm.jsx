import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { registerUser } from "../../redux/actions/authActions";
import { LoginForm } from "..";
import { Input } from '@nextui-org/react';
import {FaUserCheck} from 'react-icons/fa';
import './registerform.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
    passwordRepeat: '',
};

const RegisterForm = ({dispatch, error, needRegister}) =>{
    const navigate = useNavigate();
    const [formData, setFormData] = useState(INITIAL_STATE);
    const [stateBtn, setStateBtn] = useState(true);
    const [needRegisterUser, setNeedRegisterUser] = useState(needRegister);

    const submitForm = (ev) =>{
        ev.preventDefault();
        setFormData(INITIAL_STATE);
        dispatch(registerUser(formData));
        return navigate('/dashboard');
    };
    const changeInput = (ev) =>{
        const {name, value} = ev.target;
        const pass = document.getElementById('pass');
        const passRepeat = document.getElementById('passRepeat');
        const email = document.getElementById('email');

        setFormData({ ...formData, [name]: value});

        ((pass.value === passRepeat.value) && (pass.value.length === passRepeat.value.length) && (pass.value.length >2) && (passRepeat.value.length >2) ? setStateBtn(false) : setStateBtn(true));
        console.log(email.value.length)
    };

    return ((needRegisterUser)
            ?
            <form onSubmit={submitForm} className="register">
                <label>
                    <Input className="register-input" bordered labelPlaceholder="Correo electrónico" color="secondary" type='email' name='email' id="email" value={formData.email} onChange={changeInput}/>
                </label>
                <label>
                    <Input className="register-input" bordered labelPlaceholder="Contraseña" color="secondary" type='password' name='password' id="pass" value={formData.password} onChange={changeInput}/>
                </label>
                <label>
                    <Input className="register-input" bordered labelPlaceholder="Repetir contraseña" color="secondary" type='password' name='passwordRepeat' id="passRepeat" value={formData.passwordRepeat} onChange={changeInput}/>
                </label>
                <div>
                    <button className="register-btn" disabled={stateBtn}>Registrar</button>
                </div>
                <div className="register__login">
                <button onClick={() =>{setNeedRegisterUser(false)}}><FaUserCheck/> Ir a inicio de sesión</button>
                </div>
            </form>
            :
            <LoginForm />
    )
}
const mapStateToProps = (state) => ({
    error: state.auth.error,
});


export default connect(mapStateToProps)(RegisterForm);