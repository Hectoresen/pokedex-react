import './notFoundPage.scss';

const NotFoundPage = () =>{
    return <div className="notFound">
        <h4>¡Vaya! No hemos encontrado esta página</h4>
    </div>
}

export default NotFoundPage;