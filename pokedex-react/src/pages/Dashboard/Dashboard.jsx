import { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";
import FavPokemonContext from "../../contexts/PokemonContext";
import './dashboard.scss';


const Dashboard = (props) =>{
    const { user } = props;
    const favPokemon = useContext(FavPokemonContext);
    const [userActive, setUserActive] = useState(null);
    /* Cuando el usuario cambie, seteo */
    useEffect(() =>{
        if(user){
            setUserActive(
                {
                    email: props.user.email,
                    password: props.user.password
                }
            );
        }
    },[user])

    return(<div>
        {(userActive)
        ?
        <div className="dashboard">
                <div className="dashboard__profile">
                    Has iniciado sesión como:
                    <div className="dashboard__profile-data">
                    {userActive.email}
                </div>
            </div>
            <div className="dashboard__favpoke">
                <div className="dashboard__favpoke-header"></div>
                    <h3>Pokémons Favoritos:</h3>
                    {favPokemon.map((poke,index) =>{
                        return <div className="dashboard__favpoke-list" key={index}>Nombre: {poke.name}</div>
                    })}
            </div>
        </div>
        :
        <p>Ups! Ha ocurrido un error, trata de usar las credenciales correctas o registrarte.</p>}
    </div>)
}

export default Dashboard;