import { useEffect, React } from "react";
import { getPokemon } from '../../redux/actions/pokemonAction';
import { connect } from 'react-redux';
import { useParams, useNavigate } from "react-router-dom";
import './pokemonDetail.scss';


const PokemonDetail = (props) =>{
    const navigate = useNavigate();
    const {name} = useParams();


    useEffect(() =>{
        const {results} = props.pokemon;
        if(results && results.map(poke=> name === poke.name)){
            props.dispatch(getPokemon(name));
        }else{
            return navigate("/notfound");
        }
    }, []);

    const showPokemon = () =>{
        const {id, name, base_experience, abilities} = props.pokemonDetail;
        const {loading, error, pokemonDetail} = props;
        console.log(pokemonDetail)
        console.log(abilities)

        if(loading) return <div>Loading..</div>
        if(error) return <div>Ha ocurrido un error..</div>

        return(<div className="pokemon-detail">
            <div className="pokemon-detail__poke">
            <h2>{name}</h2>
            <div className="pokemon-detail__poke-img">
            {props.pokemonDetail.sprites && <img src={props.pokemonDetail.sprites.front_default}></img>}
            </div>
            <div className="pokemon-detail__poke-data">
            <p>Peso: <span>{pokemonDetail.weight}</span> KG </p>
            </div>
            <div className="pokemon-detail__experience">
                {(base_experience > 150)
                    ?
                <div className="pokemon-detail__experience-progress"><p>{base_experience}XP</p></div>
                    :
                <div className="pokemon-detail__experience-progress-min"><p>{base_experience}XP</p></div>}
            </div>
            <a href={`https://www.wikidex.net/wiki/${name}`} target="_blank">Ver más detalles</a>
            </div>
        </div>)
    }
    return <div>{showPokemon()}</div>
}

const mapStateToProps = ({pokemon: {loading, pokemon, pokemonDetail, error}}) =>({
    loading: loading,
    pokemon: pokemon,
    pokemonDetail: pokemonDetail,
    error: error
})

export default connect(mapStateToProps)(PokemonDetail);