import PokemonFinder from "./PokemonFinder/PokemonFinder";
import PokemonRandom from "./PokemonRandom/PokemonRandamon";
import Pokedex from './Pokedex/Pokedex';
import About from './About/About';
import Gallery from './gallery/Gallery';
import PokemonDetail from "./PokemonDetail/PokemonDetail";
import Home from './Home/Home';
import NotFoundPage from "./NotFoundPage/NotFoundPage";
import LoginForm from "./LoginForm/LoginForm";
import RegisterForm from './RegisterForm/RegisterForm'
import Dashboard from "./Dashboard/Dashboard";



export {
    PokemonFinder,
    PokemonRandom,
    Pokedex,
    About,
    Gallery,
    PokemonDetail,
    Home,
    NotFoundPage,
    LoginForm,
    RegisterForm,
    Dashboard
}