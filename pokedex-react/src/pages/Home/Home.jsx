import Carousel from 'react-bootstrap/Carousel';
import { LoginForm} from '..';
import { useState } from 'react';
import './home.scss';


const Home = () =>{
        const [index, setIndex] = useState(0);
        const handleSelect = (selectedIndex, e) => {
          setIndex(selectedIndex);
        };
        return (
          <div className='home'>
                      <Carousel activeIndex={index} onSelect={handleSelect}>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://images4.alphacoders.com/641/thumb-1920-641968.jpg"
                alt="First slide"
              />
              <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://image.winudf.com/v2/image/Y29tLnRvcHJha3Jpbmd0b25lZGV2LnBva2Vtb253YWxscGFwZXJzYW5kYmFja2dyb3VuZHBpY3R1cmVzX3NjcmVlbl8wXzE1Mjg3NTYzMDBfMDIx/screen-0.jpg?fakeurl=1&type=.jpg"
                alt="Second slide"
              />
              <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://external-preview.redd.it/NXrR_qnMRHrwUoE8pbeiX26fq4mNctFsmdEghRVApSQ.jpg?auto=webp&s=b18e1b0df84f417036d4e1ac0affb6245a071ebf"
                alt="Third slide"
              />
              <Carousel.Caption>
                <h3>Third slide label</h3>
                <p>
                  Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                </p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>

          <div className='home__users'>
            <LoginForm />
          </div>
          </div>
        );
      }

export default Home;