import Button from 'react-bootstrap/Button'
import './pokemonRandom.scss';

const PokemonRandom = () =>{
    return <div className="random">
        <Button variant="danger" size="lg" active>
            Sorpréndeme!
        </Button>{' '}
        <div className='random-img'></div>
    </div>
}

export default PokemonRandom;