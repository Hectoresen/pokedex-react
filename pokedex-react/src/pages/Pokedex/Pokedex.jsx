import { Gallery } from "..";
import './pokedex.scss';

const Pokedex = (props) =>{
    const {user} = props;
    return  <div className='main'>
                <div className='main-container'>
                    <div className='main-container__header'></div>
                    <div className="main-container__results">
                        <h1>POKEDEX</h1>
                        <Gallery user={user}/>
                    </div>
                </div>
            </div>
}
export default Pokedex;
